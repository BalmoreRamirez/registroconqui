<?php 
class InfoUsuaMdl extends CI_Model{
	public function cargarInforma($id)
	{
		$this->db->select('u.nombre1,u.nombre2,c.amigo,c.companero,c.explorador,c.orientador,c.viajero,c.guia');
		$this->db->from('t_usuarios as u');
		$this->db->join('clases as c ',' u.idUsuario = c.idUsuario');
		$this->db->where('u.idUsuario',$id);
		$data = $this->db->get();
		return $data->result_array();
	}
}

