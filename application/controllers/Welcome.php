<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$this->load->view('layouts/header');
		$this->load->view('layouts/menu');
		$this->load->view('HomeView');
		$this->load->view('layouts/footer');
	}


	public function Ingresar()
	{
		$this->load->view('layouts/header');
		$this->load->view('layouts/menu');
		$this->load->view('ingresarView');
		$this->load->view('layouts/footer');
	}

	public function Administrar()
	{
		$this->load->view('layouts/header');
		$this->load->view('layouts/menu');
		$this->load->view('adminisView');
		$this->load->view('layouts/footer');
	}

}
