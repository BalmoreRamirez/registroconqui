<?php 
class UsuarioCtr extends CI_Controller {
	public function __construct()
	{
		parent:: __construct();
		$this->load->model('UsuarioMdl');
	}
	//----------------------------------------------------------
	public function mostrarInfo()
	{
		$usuario = $this->UsuarioMdl->mostrarInf();
		foreach ($usuario as $key) {
			echo "<tr>";
			echo "<th>";
			echo $key['idUsuario'];
			echo "</th>";
			echo "<th>";
			echo $key['nombre1'];
			echo "</th>";
			echo "<th>";
			echo $key['apellido1'];
			echo "</th>";
			echo "<th>";
			echo '<a href="" class="text-info fa-2x" data-toggle="modal" 
			id="info" onclick="myFunction('.$key['idUsuario'].')"
			data-target=".bd-example-modal-lg""><i class="fa fa-info-circle"></i></a>';
			echo "</th>";
			echo "<th>";
			echo '<a href="" class="btn btn-danger btn-sm"><i class="fa fa-user-times"></i></a>';
			echo '<a href="" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></a>';
			echo "</th>";
			echo "</tr>";
		}

	}

}
?>