<?php 
class InfoUsuaCtr extends CI_Controller {
	public function __construct()
	{
		parent:: __construct();
		$this->load->model('InfoUsuaMdl');
	}
	//----------------------------------------------------------

	public function cargarInfo()
	{
		$id = $this->input->post('id');
		$info = $this->InfoUsuaMdl->cargarInforma($id);
		foreach ($info as $key) {
			echo "<tr>";
			echo "<th>";
			echo $key['nombre1'];
			echo "  ";
			echo $key['nombre2'];
			echo "</th>";
			if ($key['amigo']==1) {
				echo "<th class='text-info'><i class='fa fa-star fa-2x'></i></th>";
			}else{
				echo "<th><i class='fa fa-star fa-2x'></i></th>";
			}
			
			if ($key['companero']==1) {
				echo "<th class='text-info'><i class='fa fa-star fa-2x'></i></th>";
			}else{
				echo "<th><i class='fa fa-star fa-2x'></i></th>";
			}

			if ($key['explorador']==1) {
				echo "<th class='text-info'><i class='fa fa-star fa-2x'></i></th>";
			}else{
				echo "<th><i class='fa fa-star fa-2x'></i></th>";
			}

			if ($key['orientador']==1) {
				echo "<th class='text-info'><i class='fa fa-star fa-2x'></i></th>";
			}else{
				echo "<th><i class='fa fa-star fa-2x'></i></th>";
			}

			if ($key['viajero']==1) {
				echo "<th class='text-info'><i class='fa fa-star fa-2x'></i></th>";
			}else{
				echo "<th><i class='fa fa-star fa-2x'></i></th>";
			}

			if ($key['guia']==1) {
				echo "<th class='text-info'><i class='fa fa-star fa-2x'></i></th>";
			}else{
				echo "<th><i class='fa fa-star fa-2x'></i></th>";
			}

			


			echo "</tr>";
		}

	}
	


}