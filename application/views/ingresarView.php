<style type="text/css">
  .menu{
    background-color: #005661;
    color: white;
    font-size: 20px;
  }
  .titulo{
  	background-color: #00838f;
    color: white;
    font-size: 20px;
  }
</style>
<div class="container">
	<div class="row">
		<div class="my-3">
			<ul class="nav nav-tabs" id="myTab" role="tablist">
				<li class="nav-item">
					<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Usuario</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Clase</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Contact</a>
				</li>
			</ul>
		</div>
	</div>
	<!-------------------------->
	<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
  	<div class="row justify-content-md-center">
		<div class="col-lg-8">
			<div class="card mt-3">
				<div class="card-header text-center titulo">
					Datos personales
				</div>
				<div class="card-body">
					<form>
						<fieldset>
							<!----------------nombres--------------------->
							<div class="form-row">
								<div class="form-group col-md-6">
									<input type="text" class="form-control" id="txtnombre1" placeholder="Primer nombre">
								</div>
								<div class="form-group col-md-6">
									<input type="text" class="form-control" id="txtnombre2" placeholder="Segundo nombre">
								</div>
							</div>
							<!------------------apellidos------------------->
							<div class="form-row">
								<div class="form-group col-md-6">
									<input type="text" class="form-control" id="txtapellido1" placeholder="Primer apellido">
								</div>
								<div class="form-group col-md-6">
									<input type="text" class="form-control" id="txtapellido2" placeholder="Segundo apellido">
								</div>
							</div>
							<!------------------------------------->
							<div class="form-row">
								<div class="form-group col-md-6">
									<label>Edad</label>
									<input type="number" class="form-control" id="txtedad" placeholder="Edad">
								</div>
								<div class="form-group col-md-6">
									<label>Fecha de Nac</label>
									<input type="date" class="form-control" id="txtfecha" placeholder="Fecha de Nac">
								</div>
							</div>
							<!------------------------------------->
							<hr>
							<div class="form-group text-center">
								<button type="submit" id="btnenviar" class="btn btn-info">Enviar</button>
							</div>
							
						</fieldset>
					</form>
				</div>
			</div>
		</div>
		<!------------------------------------------------------------------------------>

	</div>
  </div>
  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">...</div>
  <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">...</div>
</div>
	
</div>	