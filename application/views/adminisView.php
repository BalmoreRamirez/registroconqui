<style type="text/css">
  .menu{
    background-color: #005661;
    color: white;
    font-size: 20px;
  }
  .titulo{
  	background-color: #00838f;
    color: white;
    font-size: 20px;
  }
</style>
<div class="container">
	<div class="row">
		<div class="my-3">
			<ul class="nav nav-tabs" id="myTab" role="tablist">
				<li class="nav-item">
					<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Usuario</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Clases</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Contact</a>
				</li>
			</ul>
		</div>
	</div>
<!------------------------------------->
<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
  	<div class="row">
		<div class="col-lg-12">
			<div class="card mt-3">
				<div class="card-header text-center titulo">
					informacion Personal
				</div>
				<div class="card-body">
					<table class="table">
						<thead>
							<tr>
								<th scope="col">#</th>
								<th scope="col">Nombre</th>
								<th scope="col">Apellido</th>
								<th scope="col">CV</th>
								<th scope="col">Acciones</th>
							</tr>
						</thead>
						<tbody id="conqui">
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
  </div>
  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">...</div>
  <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">...</div>
</div>
<!------------------------------------->

	
</div>

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">

			<table class="table">
				<thead>
					<label class="text-center">Progreso de clase progresivas</label>
					<tr>		
						<th scope="col">Nombre</th>
						<th scope="col">A</th>
						<th scope="col">C</th>
						<th scope="col">E</th>
						<th scope="col">O</th>
						<th scope="col">V</th>
						<th scope="col">G</th>
					</tr>
				</thead>
				<tbody id="infomac">

				</tbody>
			</table>
		</div>
	</div>
</div>


<!----------------mostrar informacion con js------------------------->
<script type="text/javascript">
	$(document).ready(function(){
      //cargar tabla
      infoPersonal();
  });
	function infoPersonal() {
		var url = '<?php echo base_url();?>/UsuarioCtr/mostrarInfo';
		$.ajax({ 
			type:'post',
			url:url,
			success:function(data){
				$('#conqui').html('');
				$('#conqui').html(data);
			}
		})
	}

	function myFunction(id) {
		var url = '<?php echo base_url();?>InfoUsuaCtr/cargarInfo';
		$.ajax({
			type:'post',
			url:url,
			data:'id='+id,
			success:function(data){
				$('#infomac').html('');
				$('#infomac').html(data);
			}
		}); 
	}
</script>